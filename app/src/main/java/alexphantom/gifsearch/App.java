package alexphantom.gifsearch;

import android.app.Application;

import com.bumptech.glide.Glide;

import alexphantom.gifsearch.helpers.Router;
import alexphantom.gifsearch.helpers.SingletonBus;
import lombok.Getter;

/**
 * Created by Phantom on 22.01.2017.
 */

public class App extends Application {
    private static App mApp;
    @Getter
    private Router router;
    @Getter
    private SingletonBus singletonBus;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        router = Router.getRouter();
        singletonBus = SingletonBus.getSingletonBus();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Glide.get(this).clearMemory();
        new Thread(() -> Glide.get(this).clearDiskCache()).start();
    }

    public static App getInstance() {
        return mApp;
    }
}
