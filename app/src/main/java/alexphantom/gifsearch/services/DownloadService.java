package alexphantom.gifsearch.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.parceler.Parcels;

import java.util.ArrayList;

import alexphantom.gifsearch.api.ApiService;
import alexphantom.gifsearch.model.Datum;
import alexphantom.gifsearch.model.GetGif;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static alexphantom.gifsearch.utils.Constants.BASE_URL;
import static alexphantom.gifsearch.utils.Constants.BROADCAST_RECEIVER;
import static alexphantom.gifsearch.utils.Constants.GET_TRENDING_GIF;
import static alexphantom.gifsearch.utils.Constants.KEY_BROADCAST_EXTRA;
import static alexphantom.gifsearch.utils.Constants.KEY_BROADCAST_GIF_LIST;
import static alexphantom.gifsearch.utils.Constants.KEY_QUERY;
import static alexphantom.gifsearch.utils.Constants.KEY_SORT;
import static alexphantom.gifsearch.utils.Constants.POST_URL;
import static alexphantom.gifsearch.utils.Constants.PRE_URL;
import static alexphantom.gifsearch.utils.Constants.SORT_URL;
import static alexphantom.gifsearch.utils.Constants.START_QUERY_ACTION;
import static alexphantom.gifsearch.utils.Constants.START_TRENDING_ACTION;
import static alexphantom.gifsearch.utils.Constants.TAGZ;

/**
 * Created by Phantom on 22.01.2017.
 */

public class DownloadService extends Service {

    private Retrofit retrofit;
    private Intent mBroadCastIntent;
    private Observable<GetGif> observable;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(START_TRENDING_ACTION)) {
            StartUploadThread startUploadThread = new StartUploadThread(intent, START_TRENDING_ACTION);
            new Thread(startUploadThread).start();
        } else if (intent.getAction().equals(START_QUERY_ACTION)) {
            StartUploadThread startUploadThread = new StartUploadThread(intent, START_QUERY_ACTION);
            new Thread(startUploadThread).start();
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void downloadGif(String action, String query, String sort) {
        mBroadCastIntent = new Intent(BROADCAST_RECEIVER).addCategory(Intent.CATEGORY_DEFAULT);
        ApiService api = getClient().create(ApiService.class);

        if (action.equals(START_TRENDING_ACTION)) {
            String url = BASE_URL.concat(GET_TRENDING_GIF).concat(SORT_URL).concat(sort).concat(POST_URL);
            Log.d(TAGZ, "Ola la: " + url);
            observable = api.getTrendingGif(url);
        } else if (action.equals(START_QUERY_ACTION)) {
            String url = BASE_URL.concat(PRE_URL).concat(query).concat(SORT_URL).concat(sort).concat(POST_URL);
            Log.d(TAGZ, "Ola la: " + url);
            observable = api.getSearchGif(url);
        }

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::sendBroadcastFinish,
                        error -> Log.d(TAGZ, "Failure Upload: " + error.getMessage()));
    }

    private void sendBroadcastFinish(GetGif response) {
        ArrayList<Datum> gifList = new ArrayList<>();
        for (int i = 0; i < response.getData().size(); i++) {
            Datum gif = response.getData().get(i);
            gifList.add(gif);
        }

        Bundle extra = new Bundle();
        extra.putParcelable(KEY_BROADCAST_GIF_LIST, Parcels.wrap(gifList));

        mBroadCastIntent.putExtra(KEY_BROADCAST_EXTRA, extra);
        sendBroadcast(mBroadCastIntent);
    }

    public Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
        }
        return retrofit;
    }

    class StartUploadThread implements Runnable {
        private String action;
        private Intent intent;

        StartUploadThread(Intent intent, String action) {
            this.intent = intent;
            this.action = action;
        }

        public void run() {
            String query = intent.getStringExtra(KEY_QUERY);
            String sort = intent.getStringExtra(KEY_SORT);
            downloadGif(action, query, sort);
        }
    }
}