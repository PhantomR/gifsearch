package alexphantom.gifsearch.utils;

/**
 * Created by Phantom on 22.01.2017.
 */

public class Constants {
    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";
    public static final String FRAGMENT_HEAD_TAG = "FRAGMENT_HEAD_TAG";
    public static final String FRAGMENT_BOTTOM_TAG = "FRAGMENT_BOTTOM_TAG";

    public static final String BROADCAST_RECEIVER = "BROADCAST_RECEIVER";
    public static final String KEY_BROADCAST_GIF_LIST = "KEY_BROADCAST_GIF_LIST";
    public static final String KEY_BROADCAST_EXTRA = "KEY_BROADCAST_EXTRA";

    public static final String START_TRENDING_ACTION = "START_TRENDING_ACTION";
    public static final String START_QUERY_ACTION = "START_QUERY_ACTION";

    public static final String TAGZ = "TAGZ";

    public static final String BASE_URL = "http://api.giphy.com/";
    public static final String GET_TRENDING_GIF = "v1/gifs/trending?";

    public static final String PRE_URL = "v1/gifs/search?q=";
    public static final String POST_URL = "&api_key=dc6zaTOxFJmzC";
    public static final String SORT_URL = "&rating=";

    public static final String KEY_QUERY = "KEY_QUERY";
    public static final String KEY_SORT = "KEY_SORT";

    public static final int TAB_HEIGHT = 10;
    public static final int PAGER_NUM_PAGES = 5;

    public static final CharSequence TAB_SORT_Y = "Sort Y";
    public static final CharSequence TAB_SORT_G = "Sort G";
    public static final CharSequence TAB_SORT_PG = "Sort PG";
    public static final CharSequence TAB_SORT_PG13 = "Sort PG13";
    public static final CharSequence TAB_SORT_R = "Sort R";
}
