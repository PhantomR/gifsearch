package alexphantom.gifsearch.helpers;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import alexphantom.gifsearch.R;

/**
 * Created by Phantom on 22.01.2017.
 */

public class ImageHelper {
    private static ImageHelper instance;

    public static ImageHelper getImageHelper() {
        instance = ImageHelper.getInstance();
        return instance;
    }

    private static ImageHelper getInstance() {
        if (instance == null) {
            instance = new ImageHelper();
        }
        return instance;
    }

    public void loadGif(Context context, String imageUrl, ImageView imageView, int width, int height) {
        if (imageUrl.length() > 0) {
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.rotate_item)
                    .override(width, height)
                    .error(R.drawable.no_image)
                    .into(imageView);
        }
    }
}
