package alexphantom.gifsearch.helpers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.AnimRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import alexphantom.gifsearch.R;
import alexphantom.gifsearch.services.DownloadService;
import alexphantom.gifsearch.ui.fragments.BaseFragment;

import static alexphantom.gifsearch.utils.Constants.KEY_QUERY;
import static alexphantom.gifsearch.utils.Constants.KEY_SORT;
import static alexphantom.gifsearch.utils.Constants.TAB_HEIGHT;

/**
 * Created by Phantom on 22.01.2017.
 */

public class Router {
    private static Router instance;

    private Intent initServiceIntent(Context context, String action, String query, String sort) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(action);
        intent.putExtra(KEY_QUERY, query);
        intent.putExtra(KEY_SORT, sort);
        return intent;
    }

    public static Router getRouter() {
        instance = Router.getInstance();
        return instance;
    }

    private static Router getInstance() {
        if (instance == null) {
            instance = new Router();
        }
        return instance;
    }

    public void setupPagerTabs(Context context, ViewPager viewPager, TabLayout tabLayout) {
        int tabColor = ContextCompat.getColor(context, R.color.colorGreen);
        tabLayout.setSelectedTabIndicatorColor(tabColor);
        tabLayout.setSelectedTabIndicatorHeight(TAB_HEIGHT);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void startDownloadGifService(final Context context, String action, String query, String sort) {
        context.startService(initServiceIntent(context, action, query, sort));
    }

    public void replaceFragment(FragmentActivity activity, BaseFragment fragment, int containerId,
                                String tag, @AnimRes int enter, @AnimRes int exit) {
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(enter, exit)
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commit();
    }
}
