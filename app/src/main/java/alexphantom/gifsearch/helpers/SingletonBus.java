package alexphantom.gifsearch.helpers;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class SingletonBus {

    private static SingletonBus instance;
    private Bus bus;

    private SingletonBus() {
        this.bus = new Bus(ThreadEnforcer.ANY);
    }

    public static SingletonBus getSingletonBus() {
        instance = SingletonBus.getInstance();
        return instance;
    }

    private static SingletonBus getInstance() {
        if (instance == null) {
            instance = new SingletonBus();
        }
        return instance;
    }

    public <T> void post(final T event) {
        bus.post(event);
    }

    public <T> void register(T subscriber) {
        bus.register(subscriber);
    }

    public <T> void unregister(T subscriber) {
        bus.unregister(subscriber);
    }
}
