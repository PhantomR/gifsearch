package alexphantom.gifsearch.api;

import alexphantom.gifsearch.model.GetGif;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Phantom on 22.01.2017.
 */

public interface ApiService {
    @GET
    Observable<GetGif> getTrendingGif(@Url String url);

    @GET
    Observable<GetGif> getSearchGif(@Url String url);
}
