package alexphantom.gifsearch.ui;

import android.os.Bundle;
import android.util.Log;

import alexphantom.gifsearch.R;
import alexphantom.gifsearch.ui.fragments.BaseFragment;
import alexphantom.gifsearch.ui.fragments.BottomFragment;
import alexphantom.gifsearch.ui.fragments.HeadFragment;
import alexphantom.gifsearch.ui.fragments.MainFragment;

import static alexphantom.gifsearch.utils.Constants.FRAGMENT_BOTTOM_TAG;
import static alexphantom.gifsearch.utils.Constants.FRAGMENT_HEAD_TAG;
import static alexphantom.gifsearch.utils.Constants.FRAGMENT_TAG;
import static alexphantom.gifsearch.utils.Constants.TAGZ;

/**
 * Created by Phantom on 22.01.2017.
 */

public class MainActivity extends BaseActivity implements BaseFragment.SearchCallback, BaseFragment.SortCallback {

    private String sortType;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFragments();
    }

    private void initFragments() {
        mRouter.replaceFragment(this, new HeadFragment(), R.id.head_content, FRAGMENT_HEAD_TAG,
                R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        mRouter.replaceFragment(this, new MainFragment(), R.id.main_container, FRAGMENT_TAG,
                R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        mRouter.replaceFragment(this, new BottomFragment(), R.id.bottom_container, FRAGMENT_BOTTOM_TAG,
                R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onSearchQuery(String query) {
        this.query = query;
        MainFragment mainFragment = (MainFragment)
                getSupportFragmentManager().findFragmentById(R.id.main_container);
        if (mainFragment != null) {
            mainFragment.queryRequest(query.toLowerCase(), sortType);
        }
    }

    @Override
    public void onSortCallback(int position) {
        sortType = null;
        switch (position) {
            case 0:
                sortType = "y";
                break;
            case 1:
                sortType = "g";
                break;
            case 2:
                sortType = "pg";
                break;
            case 3:
                sortType = "pg-13";
                break;
            case 4:
                sortType = "r";
                break;
        }
        Log.d(TAGZ, "sortType: " + sortType);

        MainFragment mainFragment = (MainFragment)
                getSupportFragmentManager().findFragmentById(R.id.main_container);
        if (mainFragment != null) {
            if (query != null) {
                mainFragment.queryRequest(query.toLowerCase(), sortType);
            } else {
                mainFragment.queryRequest(query, sortType);
            }

        }
    }
}
