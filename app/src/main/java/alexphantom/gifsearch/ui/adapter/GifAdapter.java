package alexphantom.gifsearch.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import alexphantom.gifsearch.R;
import alexphantom.gifsearch.helpers.ImageHelper;
import alexphantom.gifsearch.model.Datum;

/**
 * Created by Phantom on 22.01.2017.
 */

public class GifAdapter extends RecyclerView.Adapter<GifAdapter.RecyclerViewHolders> {
    private final Listener mListener;
    private List<Datum> gifList;
    private ImageHelper imageHelper;
    private Context context;

    public interface Listener {
        void noImageListener(int size);
    }

    public GifAdapter(Context context, ArrayList<Datum> offers, ImageHelper imageHelper, Listener listener) {
        this.context = context;
        this.gifList = offers;
        this.imageHelper = imageHelper;
        this.mListener = listener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gif_item, viewGroup, false);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {

        String imageUrl = gifList.get(position).getImages().getFixedHeightSmall().getUrl();
        int width = Integer.parseInt(gifList.get(position).getImages().getFixedHeightSmall().getWidth());
        int height = Integer.parseInt(gifList.get(position).getImages().getFixedHeightSmall().getHeight());

        imageHelper.loadGif(context, imageUrl, holder.imageView, width, height);

        mListener.noImageListener(gifList.size());
    }

    @Override
    public int getItemCount() {
        return gifList.size();
    }

    class RecyclerViewHolders extends RecyclerView.ViewHolder {
        private ImageView imageView;

        RecyclerViewHolders(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.gif_image);
        }
    }
}
