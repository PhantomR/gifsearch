package alexphantom.gifsearch.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import alexphantom.gifsearch.ui.pager.GifPage;

import static alexphantom.gifsearch.utils.Constants.PAGER_NUM_PAGES;
import static alexphantom.gifsearch.utils.Constants.TAB_SORT_G;
import static alexphantom.gifsearch.utils.Constants.TAB_SORT_PG;
import static alexphantom.gifsearch.utils.Constants.TAB_SORT_PG13;
import static alexphantom.gifsearch.utils.Constants.TAB_SORT_R;
import static alexphantom.gifsearch.utils.Constants.TAB_SORT_Y;

/**
 * Created by Phantom on 22.01.2017.
 */

public class SortPagerAdapter extends FragmentPagerAdapter {

    public SortPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence title = null;
        switch (position) {
            case 0:
                title = TAB_SORT_Y;
                break;
            case 1:
                title = TAB_SORT_G;
                break;
            case 2:
                title = TAB_SORT_PG;
                break;
            case 3:
                title = TAB_SORT_PG13;
                break;
            case 4:
                title = TAB_SORT_R;
                break;

        }
        return title;
    }

    @Override
    public Fragment getItem(int position) {
        return new GifPage();
    }

    @Override
    public int getCount() {
        return PAGER_NUM_PAGES;
    }
}