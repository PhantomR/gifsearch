package alexphantom.gifsearch.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import alexphantom.gifsearch.R;

/**
 * Created by Phantom on 22.01.2017.
 */

public class BottomFragment extends BaseFragment {

    private TextView textContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bottom, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textContent = (TextView) view.findViewById(R.id.text_bottom_content);
        fillData();
    }

    private void fillData() {
        String content = getResources().getString(R.string.developed_by);
        textContent.setText(content);
    }

}
