package alexphantom.gifsearch.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import alexphantom.gifsearch.R;

import static alexphantom.gifsearch.utils.Constants.TAGZ;

/**
 * Created by Phantom on 22.01.2017.
 */

public class HeadFragment extends BaseFragment {

    private EditText search;
    private SearchCallback mCallback;
    private ImageView searchButton;

    @Override
    public void onAttach(Context activity) {
        try {
            mCallback = (SearchCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement SearchCallback");
        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_head, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        search = (EditText) view.findViewById(R.id.edit_text_search);
        searchButton = (ImageView) view.findViewById(R.id.button_search);

        initClickListeners();
    }

    private void initClickListeners() {
        searchButton.setOnClickListener(v -> {
            if (search.getText().toString().length() > 0) {
                String query = search.getText().toString();

                Log.d(TAGZ, "searchFragment: " + query);

                mCallback.onSearchQuery(query);

                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                }
            }
        });
    }

    @Override
    public void onDetach() {
        mCallback = null;
        super.onDetach();
    }
}
