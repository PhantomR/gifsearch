package alexphantom.gifsearch.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import alexphantom.gifsearch.R;
import alexphantom.gifsearch.events.Event;
import alexphantom.gifsearch.ui.adapter.SortPagerAdapter;

import static alexphantom.gifsearch.utils.Constants.TAGZ;

/**
 * Created by Phantom on 22.01.2017.
 */

public class MainFragment extends BaseFragment {

    private SortPagerAdapter mAdapter;
    private ViewPager mViewPager;
    private SortCallback mCallback;

    @Override
    public void onAttach(Context activity) {
        try {
            mCallback = (SortCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement SearchCallback");
        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gif, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewPager = (ViewPager) view.findViewById(R.id.main_pager);
        TabLayout mTabLayout = (TabLayout) view.findViewById(R.id.main_pager_tabs);

        mViewPager.setAdapter(mAdapter);
        mRouter.setupPagerTabs(getActivity(), mViewPager, mTabLayout);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                Log.d(TAGZ, "onPageSelected, position = " + position);
                mCallback.onSortCallback(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter = new SortPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
    }

    public void queryRequest(String query, String sortType) {
        bus.post(new Event(query, sortType));
    }
}
