package alexphantom.gifsearch.ui.fragments;


import android.support.v4.app.Fragment;

import alexphantom.gifsearch.App;
import alexphantom.gifsearch.helpers.Router;
import alexphantom.gifsearch.helpers.SingletonBus;

/**
 * Created by Phantom on 22.01.2017.
 */

public abstract class BaseFragment extends Fragment {
    protected Router mRouter = App.getInstance().getRouter();
    protected SingletonBus bus = App.getInstance().getSingletonBus();

    public interface SearchCallback {
        void onSearchQuery(String query);
    }

    public interface SortCallback {
        void onSortCallback(int position);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
        super.onPause();
    }
}
