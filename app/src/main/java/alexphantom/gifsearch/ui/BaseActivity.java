package alexphantom.gifsearch.ui;

import android.support.v7.app.AppCompatActivity;

import alexphantom.gifsearch.App;
import alexphantom.gifsearch.helpers.Router;

/**
 * Created by Phantom on 22.01.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected Router mRouter = App.getInstance().getRouter();
}
