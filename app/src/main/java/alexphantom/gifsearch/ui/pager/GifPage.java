package alexphantom.gifsearch.ui.pager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import java.util.ArrayList;

import alexphantom.gifsearch.R;
import alexphantom.gifsearch.events.Event;
import alexphantom.gifsearch.helpers.ImageHelper;
import alexphantom.gifsearch.model.Datum;
import alexphantom.gifsearch.ui.adapter.GifAdapter;
import alexphantom.gifsearch.ui.fragments.BaseFragment;

import static alexphantom.gifsearch.utils.Constants.BROADCAST_RECEIVER;
import static alexphantom.gifsearch.utils.Constants.KEY_BROADCAST_EXTRA;
import static alexphantom.gifsearch.utils.Constants.KEY_BROADCAST_GIF_LIST;
import static alexphantom.gifsearch.utils.Constants.KEY_SORT;
import static alexphantom.gifsearch.utils.Constants.START_QUERY_ACTION;
import static alexphantom.gifsearch.utils.Constants.START_TRENDING_ACTION;
import static alexphantom.gifsearch.utils.Constants.TAGZ;

/**
 * Created by Phantom on 22.01.2017.
 */

public class GifPage extends BaseFragment {

    private IntentServiceReceiver intentServiceReceiver;
    private RecyclerView recyclerView;
    private ImageHelper imageHelper;
    private ImageView noImage;

    private GifAdapter.Listener mListener = size -> {
        if (size > 0) {
            noImage.setVisibility(View.GONE);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageHelper = ImageHelper.getImageHelper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RelativeLayout layout = (RelativeLayout) inflater
                .inflate(R.layout.page_gif, container, false);
        parseArguments();
        return layout;
    }

    private void parseArguments() {
        if (getArguments() != null) {
            if (getArguments().containsKey(KEY_SORT)) {
                String sortType = Parcels.unwrap(getArguments().getParcelable(KEY_SORT));
                Log.d("TAG", "KEY_SORT:" + sortType);
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        noImage = (ImageView) view.findViewById(R.id.no_image);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_gif);
    }

    @Override
    public void onResume() {
        super.onResume();

        registerReceiver();

        Log.d(TAGZ, "onResume: " + START_TRENDING_ACTION);
        initDownloadGifServer(START_TRENDING_ACTION, "", "y");
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(intentServiceReceiver);
    }

    private void initRV(ArrayList<Datum> gifList) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        GifAdapter adapter = new GifAdapter(getActivity(), gifList, imageHelper, mListener);
        recyclerView.setAdapter(adapter);
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter(BROADCAST_RECEIVER);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentServiceReceiver = new IntentServiceReceiver();
        getActivity().registerReceiver(intentServiceReceiver, intentFilter);
    }

    private void initDownloadGifServer(String action, String query, String sort) {
        final ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            mRouter.startDownloadGifService(getActivity(), action, query, sort);
        } else {
            Toast.makeText(getActivity(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
        }
    }


    @Subscribe
    public void onEvent(Event event) {
        Log.d(TAGZ, "===BUS=== onEvent: ");
        String query = event.getMessage();


        String sortType = event.getSort();
        if (sortType == null) {
            sortType = "y";
        }

        if (query != null) {
            Log.d(TAGZ, "===BUS=== onEvent: " + START_QUERY_ACTION);
            initDownloadGifServer(START_QUERY_ACTION, query, sortType);
        } else {
            query = "";
            Log.d(TAGZ, "===BUS=== onEvent: " + START_TRENDING_ACTION);
            initDownloadGifServer(START_TRENDING_ACTION, query, sortType);
        }
    }

    public class IntentServiceReceiver extends BroadcastReceiver {

        @SuppressWarnings("unchecked")
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extra = intent.getBundleExtra(KEY_BROADCAST_EXTRA);
            ArrayList<Datum> gifList = Parcels.unwrap(extra.getParcelable(KEY_BROADCAST_GIF_LIST));

            initRV(gifList);
        }
    }
}
