package alexphantom.gifsearch.events;

public class Sort {
    private String message;

    public Sort(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
