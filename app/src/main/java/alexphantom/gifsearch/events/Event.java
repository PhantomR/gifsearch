package alexphantom.gifsearch.events;

import lombok.Getter;

public class Event {
    @Getter
    private String message;
    @Getter
    private String sort;

    public Event(String message, String sort) {
        this.message = message;
        this.sort = sort;
    }
}
